# -*- python -*-
# ex: set syntax=python:

from buildbot.process.factory import BuildFactory

class NamedBuildFactory(BuildFactory):
    def __init__(self, name, steps=None):
        self.name = name
        BuildFactory.__init__(self, steps)

